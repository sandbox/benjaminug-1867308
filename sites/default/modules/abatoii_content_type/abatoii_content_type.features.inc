<?php
/**
 * @file
 * abatoii_content_type.features.inc
 */

/**
 * Implements hook_node_info().
 */
function abatoii_content_type_node_info() {
  $items = array(
    'toilet' => array(
      'name' => t('Toilet'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Toilet Name'),
      'help' => '',
    ),
  );
  return $items;
}
